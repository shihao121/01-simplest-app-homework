# 作业要求

## Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 程序必须能够在真实 HTTP Client 下工作。
* 必须有单元或者集成测试。每一个测试要代表一个 Task，每一个测试通过之后需要进行一次 commit（小步提交）。
* 测试至少要覆盖业务要求提到的问题。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 业务要求

现在中国的教育市场非常巨大，儿童的教（碎）育（币）投（能）资（力）得到了充分的重视。因此创业公司 “**愛颩尅谂**” 公司也立志推出面向超级小学生的教育产品。

当然做软件是非常昂贵的，因此产品也要以迭代的方式进行发布，快速试探市场的动态决定下一步的方向。目前第一期的目标如下。

作为一个小学生，我肯定非常希望快速得到一张可以打印的加法表和乘法表。这样我就可以打印下来作为参考了。我们的服务以 **API** 的形式提供。（重要，谁要是用 Page Template 的方式提供了我就 Fire 掉 TA，“愛颩尅谂” 公司的 CTO 这样说）

## Story 1 生成加法表

首先我们应当满足加法表的生成业务。目前的业务要求如下

**AC1：加法表的 API 必须以如下的形式提供**

该 API 的 Endpoint 定义如下：

|项目|说明|
|---|---|
|API Endpoint|*/api/tables/plus*|
|HTTP Method|GET|

该 API 的返回结果定义如下：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|*text/plain*|

其 Body 包含纯文本表示的加法表的全部内容。

**AC2：加法表的每一列必须都向左对齐，并且每一列和下一列的最小间隔必须为一个空格**

例如这样：

```
4+4=8
5+4=9  5+5=10
6+4=10 6+5=11 6+6=12
```

**AC3 必须打印从 1 到 9 的加法表**

## Story 2 生成乘法表

目前的业务要求如下

**AC1：乘法表的 API 必须以如下的形式提供**

该 API 的 Endpoint 定义如下：

|项目|说明|
|---|---|
|API Endpoint|*/api/tables/multiply*|
|HTTP Method|GET|

该 API 的返回结果定义如下：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|*text/plain*|

其 Body 包含纯文本表示的乘法表的全部内容。

**AC2：乘法表的每一列必须都向左对齐，并且每一列和下一列的最小间隔必须为一个空格**

例如这样：

```
2*2=4
3*2=6  3*3=9
4*2=8  4*3=12 4*4=16
5*2=10 5*3=15 5*4=20 5*5=25
```

**AC3 必须打印从 1 到 9 的乘法表**