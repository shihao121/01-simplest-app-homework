package com.twuc.webApp.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class TableUtil {

    public static List<List<String>> generate_table(OperateEnum operate, Integer start, Integer end) {
        List<List<String>> tableList = new ArrayList<>();
        Stream.iterate(start, i -> i + 1).limit(end - start + 1).forEach(row -> {
            List<String> rowTable = new ArrayList<>();
            Stream.iterate(start, i -> i + 1).limit(end - start + 1).forEach(col -> {
                if (col <= row) {
                    StringBuilder rowResult = new StringBuilder();
                    switch (operate) {
                        case PLUS:
                            rowResult.append(row).append("+").append(col).append("=").append(row + col);
                            break;
                        case MULTIPLY:
                            rowResult.append(row).append("*").append(col).append("=").append(row * col);
                    }
                    rowTable.add(rowResult.toString());
                }
            });
            tableList.add(rowTable);
        });
        return tableList;
    }
//
//    private static void formatSpace(StringBuilder result, int currentValue, int maxValue, int rowDiff) {
//        int diffLength = String.valueOf(maxValue).length() - String.valueOf(currentValue).length();
//        Stream.iterate(0, i -> i + 1).limit(diffLength + rowDiff + 1).forEach(index -> {
//            result.append(" ");
//        });
//    }
}
