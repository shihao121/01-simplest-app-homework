package com.twuc.webApp.service;

import com.twuc.webApp.utils.OperateEnum;
import com.twuc.webApp.utils.TableUtil;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TableService {

    public String createHtmlResponse(OperateEnum plus, Integer start, Integer end) {
        List<List<String>> lists = TableUtil.generate_table(plus, start, end);
        StringBuilder htmlResponse = new StringBuilder();
        htmlResponse.append("<ol>");
        lists.forEach(rowTable -> {
            htmlResponse.append("<li><ol>");
            rowTable.forEach(element -> {
                htmlResponse.append("<li>").append(element).append("</li>");
            });
            htmlResponse.append("</ol></li>");
        });
        htmlResponse.append("</ol>");
        return htmlResponse.toString();
    }
}
