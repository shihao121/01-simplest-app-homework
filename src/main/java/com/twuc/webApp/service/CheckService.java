package com.twuc.webApp.service;

import com.twuc.webApp.entity.OperateBody;
import com.twuc.webApp.entity.ResponseDto;
import org.springframework.stereotype.Service;

@Service
public class CheckService {
    public ResponseDto isCorrect(OperateBody operateBody) {
        if (operateBody.getCheckType() == null) {
            operateBody.setCheckType("=");
        }
        if (operateBody.getOperation().equals("+")) {
            return new ResponseDto(judgeCorrect(
                    operateBody.getOperandLeft() + operateBody.getOperandRight(),
                    operateBody.getExpectedResult(), operateBody.getCheckType()));
        } else {
            return new ResponseDto(judgeCorrect(
                    operateBody.getOperandLeft() * operateBody.getOperandRight(),
                    operateBody.getExpectedResult(),
                    operateBody.getCheckType()));
        }
    }

    private boolean judgeCorrect(Integer actual, Integer expect, String checkType) {
        switch (checkType) {
            case "=" : return actual.equals(expect);
            case ">" : return actual > expect;
            case "<" : return actual < expect;
        }
        return false;
    }
}
