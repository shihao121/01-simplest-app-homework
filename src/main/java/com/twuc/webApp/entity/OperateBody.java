package com.twuc.webApp.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class OperateBody {

    @NotNull
    private Integer operandLeft;

    @NotNull
    private Integer operandRight;

    @NotNull
    private String operation;

    @NotNull
    private Integer expectedResult;

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    private String checkType;

    public OperateBody(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = "=";
    }

    public OperateBody(Integer operandLeft, Integer operandRight, String operation, String checkType, Integer expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = checkType;
    }

    public OperateBody() {
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    @Override
    public String toString() {
        return "OperateBody{" +
                "operandLeft=" + operandLeft +
                ", operandRight=" + operandRight +
                ", operation='" + operation + '\'' +
                ", expectedResult=" + expectedResult +
                ", checkType='" + checkType + '\'' +
                '}';
    }
}
