package com.twuc.webApp.entity;

public class ResponseDto {

    private Boolean correct;

    public ResponseDto(Boolean correct) {
        this.correct = correct;
    }

    public ResponseDto() {
    }

    public Boolean getCorrect() {
        return correct;
    }

    @Override
    public String toString() {
        return "ResponseDto{" +
                "correct=" + correct +
                '}';
    }
}
