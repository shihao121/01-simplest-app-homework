package com.twuc.webApp.controller;

import com.twuc.webApp.entity.OperateBody;
import com.twuc.webApp.entity.ResponseDto;
import com.twuc.webApp.service.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CheckController {

    private final CheckService checkService;

    public CheckController(CheckService checkService) {
        this.checkService = checkService;
    }

    @PostMapping(value = "/api/check")
    public ResponseDto isCorrect(@RequestBody @Valid OperateBody operateBody){

        return checkService.isCorrect(operateBody);

    }


}
