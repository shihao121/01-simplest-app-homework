package com.twuc.webApp.controller;

import com.twuc.webApp.service.TableService;
import com.twuc.webApp.utils.OperateEnum;
import com.twuc.webApp.utils.TableUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController()
public class TableController {

    @Autowired
    private TableService tableService;

    @GetMapping(value = "api/tables/plus")
    public ResponseEntity getPlusTables(@RequestParam(defaultValue = "1") Integer start,
                                        @RequestParam(defaultValue = "9") Integer end,
                                        HttpServletRequest request) {
        if ("json".equals(request.getHeader("resp-style"))) {
            return ResponseEntity.status(HttpStatus.OK).body(TableUtil.generate_table(OperateEnum.PLUS, start, end));
        }
        if ("html".equals(request.getHeader("resp-style"))) {
            return ResponseEntity.ok(tableService.createHtmlResponse(OperateEnum.PLUS, start, end));
        }
        return null;
    }

    @GetMapping(value = "api/tables/multiply")
    public ResponseEntity getMultiplyTables(@RequestParam(defaultValue = "1") Integer start,
                                            @RequestParam(defaultValue = "9") Integer end,
                                            HttpServletRequest request) {
        if ("json".equals(request.getHeader("resp-style"))) {
            return ResponseEntity.status(HttpStatus.OK).body(TableUtil.generate_table(OperateEnum.MULTIPLY, start, end));
        }
        if ("html".equals(request.getHeader("resp-style"))) {
            return ResponseEntity.ok(tableService.createHtmlResponse(OperateEnum.MULTIPLY, start, end));
        }
        return null;

    }

}
