package com.twuc.webApp.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TableUtilTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void should_return_plus_table() throws JsonProcessingException {
        List<List<String>> plusTable = TableUtil.generate_table(OperateEnum.PLUS, 1, 9);
        assertEquals("[[\"1+1=2\"]," +
                "[\"2+1=3\",\"2+2=4\"]," +
                "[\"3+1=4\",\"3+2=5\",\"3+3=6\"]," +
                "[\"4+1=5\",\"4+2=6\",\"4+3=7\",\"4+4=8\"]," +
                "[\"5+1=6\",\"5+2=7\",\"5+3=8\",\"5+4=9\",\"5+5=10\"]," +
                "[\"6+1=7\",\"6+2=8\",\"6+3=9\",\"6+4=10\",\"6+5=11\",\"6+6=12\"]," +
                "[\"7+1=8\",\"7+2=9\",\"7+3=10\",\"7+4=11\",\"7+5=12\",\"7+6=13\",\"7+7=14\"]," +
                "[\"8+1=9\",\"8+2=10\",\"8+3=11\",\"8+4=12\",\"8+5=13\",\"8+6=14\",\"8+7=15\",\"8+8=16\"]," +
                "[\"9+1=10\",\"9+2=11\",\"9+3=12\",\"9+4=13\",\"9+5=14\",\"9+6=15\",\"9+7=16\",\"9+8=17\",\"9+9=18\"]]", objectMapper.writeValueAsString(plusTable));
    }

    @Test
    void should_return_multiply_table() throws JsonProcessingException {
        List<List<String>> multiplyTable = TableUtil.generate_table(OperateEnum.MULTIPLY, 1, 9);
        assertEquals("[[\"1*1=1\"]," +
                "[\"2*1=2\",\"2*2=4\"]," +
                "[\"3*1=3\",\"3*2=6\",\"3*3=9\"]," +
                "[\"4*1=4\",\"4*2=8\",\"4*3=12\",\"4*4=16\"]," +
                "[\"5*1=5\",\"5*2=10\",\"5*3=15\",\"5*4=20\",\"5*5=25\"]," +
                "[\"6*1=6\",\"6*2=12\",\"6*3=18\",\"6*4=24\",\"6*5=30\",\"6*6=36\"]," +
                "[\"7*1=7\",\"7*2=14\",\"7*3=21\",\"7*4=28\",\"7*5=35\",\"7*6=42\",\"7*7=49\"]," +
                "[\"8*1=8\",\"8*2=16\",\"8*3=24\",\"8*4=32\",\"8*5=40\",\"8*6=48\",\"8*7=56\",\"8*8=64\"]," +
                "[\"9*1=9\",\"9*2=18\",\"9*3=27\",\"9*4=36\",\"9*5=45\",\"9*6=54\",\"9*7=63\",\"9*8=72\",\"9*9=81\"]]",
                objectMapper.writeValueAsString(multiplyTable));
    }
}