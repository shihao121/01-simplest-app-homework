package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.entity.OperateBody;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class CheckControllerIntegrationTest {
    
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_200_correct_message_given_requestBody() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"operandLeft\": 1,\n" +
                        "  \"operandRight\": 1,\n" +
                        "  \"operation\": \"+\",\n" +
                        "  \"expectedResult\": 2\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("{\"correct\":true}"));
    }

    @Test
    void should_return_400_when_any_param_is_null() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"operandLeft\": 1,\n" +
                        "  \"operandRight\": 1,\n" +
                        "  \"expectedResult\": 2\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_and_correct_message_given_1plus1_equals_2() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String operateJson = objectMapper.writeValueAsString(new OperateBody(1, 1, "+", "=", 2));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(operateJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("{\"correct\":true}"));
    }

    @Test
    void should_return_200_correct_message_given_1multiply2_greater_than_1() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String operateJson = objectMapper.writeValueAsString(new OperateBody(1, 2, "*", ">", 1));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(operateJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("{\"correct\":true}"));
    }

    @Test
    void should_return_200_defeat_message_given_7multiply2_less_than10() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String operateJson = objectMapper.writeValueAsString(new OperateBody(7,
                2, "*", "<", 10));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(operateJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("{\"correct\":false}"));

    }

    @Test
    void should_return_400__given_param_is_not_specify_type() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"operandLeft\": \"haha\",\n" +
                        "  \"operandRight\": 1,\n" +
                        "  \"operation\": \"+\",\n" +
                        "  \"expectedResult\": 2\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}