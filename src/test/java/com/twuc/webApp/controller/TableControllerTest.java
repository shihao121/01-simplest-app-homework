package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.service.TableService;
import com.twuc.webApp.utils.OperateEnum;
import com.twuc.webApp.utils.TableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class TableControllerTest {

    @Autowired
    MockMvc mockMvc;

    private TableService tableService = new TableService();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void should_return_9x9plus_table_when_get_tables_plus() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus")
                .header("resp-style", "json"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(TableUtil.generate_table(OperateEnum.PLUS, 1, 9))));

    }

    @Test
    void should_return_9x9multiply_table_when_get_tables_multiply() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply")
                .header("resp-style", "json"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(TableUtil.generate_table(OperateEnum.MULTIPLY, 1, 9))));
    }

    @Test
    void should_return_4x4plus_tables_given_start4_end_7() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus?start=4&end=7")
                .header("resp-style", "json"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(TableUtil.generate_table(OperateEnum.PLUS, 4, 7))));
    }

    @Test
    void should_return_4x4multiply_tables_given_start4_end_7() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply?start=4&end=7")
                .header("resp-style", "json"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(TableUtil.generate_table(OperateEnum.MULTIPLY, 4, 7))));
    }

    @Test
    void should_return_json_format_response_given_request_header_resp_style_json() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply?start=4&end=7")
                .header("resp-style", "json"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(TableUtil.generate_table(OperateEnum.MULTIPLY, 4, 7))));
    }

    @Test
    void should_return_html_format_response_given_request_header_resp_style_html() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus?start=4&end=7")
                .header("resp-style", "html"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string(tableService.
                        createHtmlResponse(OperateEnum.PLUS, 4, 7)));
    }

    @Test
    void should_return_html_format_9multiply9_table_response_given_request_header_resp_style_html() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply?start=4&end=7")
                .header("resp-style", "html"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string(tableService.
                        createHtmlResponse(OperateEnum.MULTIPLY, 4, 7)));
    }
}