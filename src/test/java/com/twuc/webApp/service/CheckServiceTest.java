package com.twuc.webApp.service;

import com.twuc.webApp.entity.OperateBody;
import com.twuc.webApp.entity.ResponseDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckServiceTest {

    private CheckService service = new CheckService();

    @Test
    void should_return_correct_when_1plus1_equals2() {
        ResponseDto correct = service.isCorrect(new OperateBody(1, 1, "+", "=", 2));
        assertEquals(true, correct.getCorrect());
    }

    @Test
    void should_return_correct_when_1multiply2_less_than3() {
        ResponseDto correct = service.isCorrect(new OperateBody(1, 2, "*", "<", 3));
        assertEquals(true, correct.getCorrect());
    }

    @Test
    void should_return_false_when_3multiply3_grater_than9() {
        ResponseDto correct = service.isCorrect(new OperateBody(3, 3, "*", ">", 9));
        assertEquals(false, correct.getCorrect());
    }
}