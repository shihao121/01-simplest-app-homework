package com.twuc.webApp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.twuc.webApp.utils.OperateEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TableServiceTest {

    private TableService tableService = new TableService();

    @Test
    void should_return_html_9plus9table_response() {
        String htmlResponse = tableService.createHtmlResponse(OperateEnum.PLUS, 3, 5);
        assertEquals("<ol>" +
                "<li>" +
                "<ol>" +
                "<li>3+3=6</li>" +
                "</ol>" +
                "</li>" +
                "<li>" +
                "<ol>" +
                "<li>4+3=7</li>" +
                "<li>4+4=8</li>" +
                "</ol>" +
                "</li>" +
                "<li>" +
                "<ol>" +
                "<li>5+3=8</li>" +
                "<li>5+4=9</li>" +
                "<li>5+5=10</li>" +
                "</ol>" +
                "</li>" +
                "</ol>", htmlResponse);
    }

    @Test
    void should_return_html_9multiply9table_response() {
        String htmlResponse = tableService.createHtmlResponse(OperateEnum.MULTIPLY, 3, 5);
        assertEquals("<ol>" +
                "<li>" +
                "<ol>" +
                "<li>3*3=9</li>" +
                "</ol>" +
                "</li>" +
                "<li>" +
                "<ol>" +
                "<li>4*3=12</li>" +
                "<li>4*4=16</li>" +
                "</ol>" +
                "</li>" +
                "<li>" +
                "<ol>" +
                "<li>5*3=15</li>" +
                "<li>5*4=20</li>" +
                "<li>5*5=25</li>" +
                "</ol>" +
                "</li>" +
                "</ol>", htmlResponse);
    }
}